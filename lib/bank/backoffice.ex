defmodule Bank.Backoffice do
  @moduledoc """
  The Backoffice context.
  """

  import Ecto.Query, warn: false
  alias Bank.Backoffice.UserBackoffice
  alias Bank.Backoffice.UserBackofficeNotifier
  alias Bank.Backoffice.UserBackofficeToken
  alias Bank.Repo

  ## Database getters

  @doc """
  Gets a user_backoffice by email.

  ## Examples

      iex> get_user_backoffice_by_email("foo@example.com")
      %UserBackoffice{}

      iex> get_user_backoffice_by_email("unknown@example.com")
      nil

  """
  def get_user_backoffice_by_email(email) when is_binary(email) do
    Repo.get_by(UserBackoffice, email: email)
  end

  @doc """
  Gets a user_backoffice by email and password.

  ## Examples

      iex> get_user_backoffice_by_email_and_password("foo@example.com", "correct_password")
      %UserBackoffice{}

      iex> get_user_backoffice_by_email_and_password("foo@example.com", "invalid_password")
      nil

  """
  def get_user_backoffice_by_email_and_password(email, password)
      when is_binary(email) and is_binary(password) do
    user_backoffice = Repo.get_by(UserBackoffice, email: email)
    if UserBackoffice.valid_password?(user_backoffice, password), do: user_backoffice
  end

  @doc """
  Gets a single user_backoffice.

  Raises `Ecto.NoResultsError` if the UserBackoffice does not exist.

  ## Examples

      iex> get_user_backoffice!(123)
      %UserBackoffice{}

      iex> get_user_backoffice!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_backoffice!(id), do: Repo.get!(UserBackoffice, id)

  ## User backoffice registration

  @doc """
  Registers a user_backoffice.

  ## Examples

      iex> register_user_backoffice(%{field: value})
      {:ok, %UserBackoffice{}}

      iex> register_user_backoffice(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def register_user_backoffice(attrs) do
    %UserBackoffice{}
    |> UserBackoffice.registration_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_backoffice changes.

  ## Examples

      iex> change_user_backoffice_registration(user_backoffice)
      %Ecto.Changeset{data: %UserBackoffice{}}

  """
  def change_user_backoffice_registration(%UserBackoffice{} = user_backoffice, attrs \\ %{}) do
    UserBackoffice.registration_changeset(user_backoffice, attrs, hash_password: false)
  end

  ## Settings

  @doc """
  Returns an `%Ecto.Changeset{}` for changing the user_backoffice email.

  ## Examples

      iex> change_user_backoffice_email(user_backoffice)
      %Ecto.Changeset{data: %UserBackoffice{}}

  """
  def change_user_backoffice_email(user_backoffice, attrs \\ %{}) do
    UserBackoffice.email_changeset(user_backoffice, attrs)
  end

  @doc """
  Emulates that the email will change without actually changing
  it in the database.

  ## Examples

      iex> apply_user_backoffice_email(user_backoffice, "valid password", %{email: ...})
      {:ok, %UserBackoffice{}}

      iex> apply_user_backoffice_email(user_backoffice, "invalid password", %{email: ...})
      {:error, %Ecto.Changeset{}}

  """
  def apply_user_backoffice_email(user_backoffice, password, attrs) do
    user_backoffice
    |> UserBackoffice.email_changeset(attrs)
    |> UserBackoffice.validate_current_password(password)
    |> Ecto.Changeset.apply_action(:update)
  end

  @doc """
  Updates the user_backoffice email using the given token.

  If the token matches, the user_backoffice email is updated and the token is deleted.
  The confirmed_at date is also updated to the current time.
  """
  def update_user_backoffice_email(user_backoffice, token) do
    context = "change:#{user_backoffice.email}"

    with {:ok, query} <- UserBackofficeToken.verify_change_email_token_query(token, context),
         %UserBackofficeToken{sent_to: email} <- Repo.one(query),
         {:ok, _} <-
           Repo.transaction(user_backoffice_email_multi(user_backoffice, email, context)) do
      :ok
    else
      _ -> :error
    end
  end

  defp user_backoffice_email_multi(user_backoffice, email, context) do
    changeset =
      user_backoffice
      |> UserBackoffice.email_changeset(%{email: email})
      |> UserBackoffice.confirm_changeset()

    Ecto.Multi.new()
    |> Ecto.Multi.update(:user_backoffice, changeset)
    |> Ecto.Multi.delete_all(
      :tokens,
      UserBackofficeToken.user_backoffice_and_contexts_query(user_backoffice, [context])
    )
  end

  @doc """
  Delivers the update email instructions to the given user_backoffice.

  ## Examples

      iex> deliver_update_email_instructions(user_backoffice, current_email, &Routes.user_backoffice_update_email_url(conn, :edit, &1))
      {:ok, %{to: ..., body: ...}}

  """
  def deliver_update_email_instructions(
        %UserBackoffice{} = user_backoffice,
        current_email,
        update_email_url_fun
      )
      when is_function(update_email_url_fun, 1) do
    {encoded_token, user_backoffice_token} =
      UserBackofficeToken.build_email_token(user_backoffice, "change:#{current_email}")

    Repo.insert!(user_backoffice_token)

    UserBackofficeNotifier.deliver_update_email_instructions(
      user_backoffice,
      update_email_url_fun.(encoded_token)
    )
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for changing the user_backoffice password.

  ## Examples

      iex> change_user_backoffice_password(user_backoffice)
      %Ecto.Changeset{data: %UserBackoffice{}}

  """
  def change_user_backoffice_password(user_backoffice, attrs \\ %{}) do
    UserBackoffice.password_changeset(user_backoffice, attrs, hash_password: false)
  end

  @doc """
  Updates the user_backoffice password.

  ## Examples

      iex> update_user_backoffice_password(user_backoffice, "valid password", %{password: ...})
      {:ok, %UserBackoffice{}}

      iex> update_user_backoffice_password(user_backoffice, "invalid password", %{password: ...})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_backoffice_password(user_backoffice, password, attrs) do
    changeset =
      user_backoffice
      |> UserBackoffice.password_changeset(attrs)
      |> UserBackoffice.validate_current_password(password)

    Ecto.Multi.new()
    |> Ecto.Multi.update(:user_backoffice, changeset)
    |> Ecto.Multi.delete_all(
      :tokens,
      UserBackofficeToken.user_backoffice_and_contexts_query(user_backoffice, :all)
    )
    |> Repo.transaction()
    |> case do
      {:ok, %{user_backoffice: user_backoffice}} -> {:ok, user_backoffice}
      {:error, :user_backoffice, changeset, _} -> {:error, changeset}
    end
  end

  ## Session

  @doc """
  Generates a session token.
  """
  def generate_user_backoffice_session_token(user_backoffice) do
    {token, user_backoffice_token} = UserBackofficeToken.build_session_token(user_backoffice)
    Repo.insert!(user_backoffice_token)
    token
  end

  @doc """
  Gets the user_backoffice with the given signed token.
  """
  def get_user_backoffice_by_session_token(token) do
    {:ok, query} = UserBackofficeToken.verify_session_token_query(token)
    Repo.one(query)
  end

  @doc """
  Deletes the signed token with the given context.
  """
  def delete_session_token(token) do
    Repo.delete_all(UserBackofficeToken.token_and_context_query(token, "session"))
    :ok
  end

  ## Confirmation

  @doc """
  Delivers the confirmation email instructions to the given user_backoffice.

  ## Examples

      iex> deliver_user_backoffice_confirmation_instructions(user_backoffice, &Routes.user_backoffice_confirmation_url(conn, :confirm, &1))
      {:ok, %{to: ..., body: ...}}

      iex> deliver_user_backoffice_confirmation_instructions(confirmed_user_backoffice, &Routes.user_backoffice_confirmation_url(conn, :confirm, &1))
      {:error, :already_confirmed}

  """
  def deliver_user_backoffice_confirmation_instructions(
        %UserBackoffice{} = user_backoffice,
        confirmation_url_fun
      )
      when is_function(confirmation_url_fun, 1) do
    if user_backoffice.confirmed_at do
      {:error, :already_confirmed}
    else
      {encoded_token, user_backoffice_token} =
        UserBackofficeToken.build_email_token(user_backoffice, "confirm")

      Repo.insert!(user_backoffice_token)

      UserBackofficeNotifier.deliver_confirmation_instructions(
        user_backoffice,
        confirmation_url_fun.(encoded_token)
      )
    end
  end

  @doc """
  Confirms a user_backoffice by the given token.

  If the token matches, the user_backoffice account is marked as confirmed
  and the token is deleted.
  """
  def confirm_user_backoffice(token) do
    with {:ok, query} <- UserBackofficeToken.verify_email_token_query(token, "confirm"),
         %UserBackoffice{} = user_backoffice <- Repo.one(query),
         {:ok, %{user_backoffice: user_backoffice}} <-
           Repo.transaction(confirm_user_backoffice_multi(user_backoffice)) do
      {:ok, user_backoffice}
    else
      _ -> :error
    end
  end

  defp confirm_user_backoffice_multi(user_backoffice) do
    Ecto.Multi.new()
    |> Ecto.Multi.update(:user_backoffice, UserBackoffice.confirm_changeset(user_backoffice))
    |> Ecto.Multi.delete_all(
      :tokens,
      UserBackofficeToken.user_backoffice_and_contexts_query(user_backoffice, ["confirm"])
    )
  end

  ## Reset password

  @doc """
  Delivers the reset password email to the given user_backoffice.

  ## Examples

      iex> deliver_user_backoffice_reset_password_instructions(user_backoffice, &Routes.user_backoffice_reset_password_url(conn, :edit, &1))
      {:ok, %{to: ..., body: ...}}

  """
  def deliver_user_backoffice_reset_password_instructions(
        %UserBackoffice{} = user_backoffice,
        reset_password_url_fun
      )
      when is_function(reset_password_url_fun, 1) do
    {encoded_token, user_backoffice_token} =
      UserBackofficeToken.build_email_token(user_backoffice, "reset_password")

    Repo.insert!(user_backoffice_token)

    UserBackofficeNotifier.deliver_reset_password_instructions(
      user_backoffice,
      reset_password_url_fun.(encoded_token)
    )
  end

  @doc """
  Gets the user_backoffice by reset password token.

  ## Examples

      iex> get_user_backoffice_by_reset_password_token("validtoken")
      %UserBackoffice{}

      iex> get_user_backoffice_by_reset_password_token("invalidtoken")
      nil

  """
  def get_user_backoffice_by_reset_password_token(token) do
    with {:ok, query} <- UserBackofficeToken.verify_email_token_query(token, "reset_password"),
         %UserBackoffice{} = user_backoffice <- Repo.one(query) do
      user_backoffice
    else
      _ -> nil
    end
  end

  @doc """
  Resets the user_backoffice password.

  ## Examples

      iex> reset_user_backoffice_password(user_backoffice, %{password: "new long password", password_confirmation: "new long password"})
      {:ok, %UserBackoffice{}}

      iex> reset_user_backoffice_password(user_backoffice, %{password: "valid", password_confirmation: "not the same"})
      {:error, %Ecto.Changeset{}}

  """
  def reset_user_backoffice_password(user_backoffice, attrs) do
    Ecto.Multi.new()
    |> Ecto.Multi.update(
      :user_backoffice,
      UserBackoffice.password_changeset(user_backoffice, attrs)
    )
    |> Ecto.Multi.delete_all(
      :tokens,
      UserBackofficeToken.user_backoffice_and_contexts_query(user_backoffice, :all)
    )
    |> Repo.transaction()
    |> case do
      {:ok, %{user_backoffice: user_backoffice}} -> {:ok, user_backoffice}
      {:error, :user_backoffice, changeset, _} -> {:error, changeset}
    end
  end
end
