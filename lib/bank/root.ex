defmodule Bank.Root do
  @moduledoc """
  Context to root actions.
  """

  alias Bank.Accounts

  def add_credit(account_id, amount) do
    root = Accounts.get_root_account()

    Accounts.create_transference(%{
      amount: amount,
      description: "Root given",
      source_account_id: root.id,
      destination_account_id: account_id
    })
  end
end
