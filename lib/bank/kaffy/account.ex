defmodule Bank.Kaffy.Account do
  @moduledoc """
  Admin for accounts
  """

  import Ecto.Query, warn: false

  defp uuid?(search) do
    case Ecto.UUID.cast(search) do
      {:ok, _} -> true
      :error -> false
    end
  end

  def custom_index_query(conn, _schema, query) do
    do_custom_index_query(conn.query_params, query)
  end

  def do_custom_index_query(%{"search" => ""}, query), do: query

  def do_custom_index_query(%{"search" => search}, query) do
    query =
      if uuid?(search) do
        where(query, [account], account.id == ^search)
      else
        query
      end

    query =
      if AccountEnum.valid_value?(search) do
        from account in query,
          or_where: account.type == ^search
      else
        query
      end

    query
  end

  def do_custom_index_query(%{}, query), do: query
end
