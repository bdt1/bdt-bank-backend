defmodule Bank.Kaffy.Transaction do
  @moduledoc """
  Admin for transactions
  """

  import Ecto.Query, warn: false

  defp uuid?(search) do
    case Ecto.UUID.cast(search) do
      {:ok, _} -> true
      :error -> false
    end
  end

  def custom_index_query(conn, _schema, query) do
    do_custom_index_query(conn.query_params, query)
  end

  def do_custom_index_query(%{"search" => ""}, query), do: query

  def do_custom_index_query(%{"search" => search}, query) do
    query =
      if uuid?(search) do
        from transaction in query,
          or_where: transaction.id == ^search,
          join: source_account in assoc(transaction, :source_account),
          or_where: source_account.id == ^search,
          join: destination_account in assoc(transaction, :destination_account),
          or_where: destination_account.id == ^search
      else
        query
      end

    query =
      if TransactionEnum.valid_value?(search) do
        from transaction in query,
          or_where: transaction.type == ^search
      else
        query
      end

    query
  end

  def do_custom_index_query(%{}, query), do: query
end
