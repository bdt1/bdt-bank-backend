defmodule Bank.Kaffy.User do
  @moduledoc """
  Admin for users
  """

  import Ecto.Query, warn: false

  alias BankWeb.Endpoint
  alias BankWeb.Router.Helpers, as: Routes

  def custom_links(_schema) do
    [
      %{
        name: "Logout",
        url: Routes.user_backoffice_session_path(Endpoint, :delete),
        order: 2,
        method: "DELETE",
        location: :top,
        icon: "sign-out-alt"
      }
    ]
  end

  def create_changeset(schema, attrs) do
    attrs =
      case attrs["account_id"] do
        "" -> Map.delete(attrs, "account_id")
        nil -> attrs
        _ -> attrs
    end
    Bank.Accounts.User.changeset(schema, attrs)
  end

  defp uuid?(search) do
    case Ecto.UUID.cast(search) do
      {:ok, _} -> true
      :error -> false
    end
  end

  def custom_index_query(conn, _schema, query) do
    do_custom_index_query(conn.query_params, query)
  end

  def do_custom_index_query(%{"search" => ""}, query), do: query

  def do_custom_index_query(%{"search" => search}, query) do
    if uuid?(search) do
      from user in query,
        or_where: user.id == ^search,
        join: account in assoc(user, :account),
        or_where: account.id == ^search
    else
      query
    end
  end

  def do_custom_index_query(%{}, query), do: query
end
