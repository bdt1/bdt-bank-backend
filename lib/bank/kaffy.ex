defmodule Bank.Kaffy do
  @moduledoc """
  Kaffy admin configs.
  """
  def create_resources(_conn) do
    [
      accounts: [
        resources: [
          transaction: [schema: Bank.Accounts.Transaction, admin: Bank.Kaffy.Transaction],
          user: [schema: Bank.Accounts.User, admin: Bank.Kaffy.User]        ]
      ]
    ]
  end
end
