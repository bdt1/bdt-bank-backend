defmodule Bank.Pagination do
  @moduledoc """
  Generic offset-based pagination for list queries
  """
  import Ecto.Query
  alias Bank.Repo

  defp query(query, page, per_page: per_page) do
    query
    |> limit(^(per_page + 1))
    |> offset(^(per_page * (page - 1)))
    |> order_by([desc: :inserted_at])
    |> Repo.all()
  end

  defp count(query) do
    query =
      query
      |> exclude(:preload)
      |> exclude(:order_by)
    from t in query,
      select: count("*")
  end

  def page(query, page, per_page: per_page) when is_binary(page),
    do: page(query, String.to_integer(page), per_page: per_page)

  def page(query, page, per_page: per_page) when is_binary(per_page),
    do: page(query, page, per_page: String.to_integer(per_page))

  def page(query, page, per_page: per_page)
      when is_integer(page) and page > 0 and is_integer(per_page) do
    results = query(query, page, per_page: per_page)
    has_next = length(results) > per_page
    has_prev = page > 1
    count = Repo.one(count(query))

    {:ok,
     %{
       has_next: has_next,
       has_prev: has_prev,
       prev_page: page - 1,
       page: page,
       next_page: page + 1,
       first_index: (page - 1) * per_page + 1,
       last_index: Enum.min([page * per_page, count]),
       count: count,
       list: Enum.slice(results, 0, per_page)
     }}
  end
end
