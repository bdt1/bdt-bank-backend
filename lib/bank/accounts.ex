defmodule Bank.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Bank.Repo
  alias Ecto.Multi

  alias Bank.Accounts.Account
  alias Bank.Accounts.Transaction
  alias Bank.Accounts.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    User |> Repo.all() |> Repo.preload(:account)
  end

  def get_user_autocomplete(field) do
    field = "%#{field}%"
    query =
      from user in User,
        where: ilike(user.name, ^field),
        limit: 15,
        preload: :account
    Repo.all(query)
  end
  @doc """
  Gets a single user.

  Return {:error, :not_found} if the User does not exist.

  ## Examples

      iex> get_user("13f81095-38c4-4932-9fa8-4c5585b46c74")
      %User{}

      iex> get_user("inexistent_uuid")
      {:error, :not_found}

  """
  def get_user(id) when is_binary(id) do
    User
    |> Repo.get(id)
    |> get_user()
  rescue
    Ecto.Query.CastError -> get_user(nil)
  end

  def get_user(%User{} = user), do: {:ok, Repo.preload(user, :account)}
  def get_user(nil), do: {:error, :not_found}

  def get_account(id) when is_binary(id) do
    Account
    |> Repo.get(id)
    |> get_account()
  rescue
    Ecto.Query.CastError -> get_account(nil)
  end

  def get_account(%Account{} = account), do: {:ok, account}
  def get_account(nil), do: {:error, :not_found}

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(Map.merge(attrs, %{account: %{}}))
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  def credit(account_id) do
    query =
      from acc in Account,
        where: acc.id == ^account_id,
        left_join: credits in assoc(acc, :destination_transactions),
        select: coalesce(sum(credits.amount), 0)
    Repo.one(query)
  end

  def debit(account_id) do
    query =
      from acc in Account,
        where: acc.id == ^account_id,
        left_join: debits in assoc(acc, :source_transactions),
        select: coalesce(sum(debits.amount), 0)
    Repo.one(query)
  end

  def balance(account_id) do
    credit(account_id) - debit(account_id)
  end

  defp normal_account?(account_id) do
    case get_account(account_id) do
      {:ok, account} -> account.type == :normal
      {:error, changeset} -> {:error, changeset}
    end
  end

  # This validation assumes the transaction was created without another errors
  # So the balance is already updated with transaction amount thus it cannot be lesser than 0
  defp validate_balance(%Transaction{} = transaction) do
    account_id = transaction.source_account_id

    if normal_account?(account_id) &&
         balance(account_id) < 0 do
      {:error,
       Ecto.Changeset.add_error(
         Transaction.changeset(%Transaction{}, Map.from_struct(transaction)),
         :amount,
         "insuficient credit"
       )}
    else
      {:ok, transaction}
    end
  end

  def create_transaction(attrs \\ %{}) do
    Multi.new()
    |> Multi.insert(
      :transaction,
      Transaction.changeset(%Transaction{}, attrs)
    )
    |> Multi.run(:validate_balance, fn _, %{transaction: transaction} ->
      validate_balance(transaction)
    end)
    |> Repo.transaction()
  end

  def create_transference(attrs \\ %{}) do
    attrs
    |> Map.put(:type, :transfer)
    |> create_transaction()
  end

  def get_root_account do
    Repo.get_by(Account, type: :root)
  end
end
