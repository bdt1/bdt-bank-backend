import EctoEnum

# Application defined enums
defenum(TransactionEnum, transfer: 0, reversal: 1)
defenum(AccountEnum, root: 0, normal: 1)
