defmodule Bank.Backoffice.UserBackofficeAdmin do
  @moduledoc """
  Admin for backoffice users.
  """

  alias BankWeb.Endpoint
  alias BankWeb.Router.Helpers, as: Routes

  def custom_links(_schema) do
    [
      %{
        name: "Logout",
        url: Routes.user_backoffice_session_path(Endpoint, :delete),
        order: 2,
        method: "DELETE",
        location: :top,
        icon: "sign-out-alt"
      }
    ]
  end
end
