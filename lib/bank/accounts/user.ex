defmodule Bank.Accounts.User do
  @moduledoc """
  User schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Bank.Accounts.Account

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :name, :string
    belongs_to :account, Account

    timestamps()
  end

  defp account_id_changeset(user, attrs) do
    user
    |> cast(attrs, [:name] ++ [:account_id])
    |> validate_required([:name] ++ [:account_id])
    |> unique_constraint(:account_id)
  end

  def create_account(attrs) do
    attrs
    |> Bank.Helper.Map.atomize_keys()
    |> Map.put(:account, %{})
  end
  @doc false
  def changeset(user, attrs) do
    case attrs do
      %{account_id: _} ->
        account_id_changeset(user, attrs)

      %{"account_id" => _} ->
        account_id_changeset(user, attrs)

      _ ->
        user
        |> cast(create_account(attrs), [:name])
        |> validate_required([:name])
        |> cast_assoc(:account, required: true)
    end
  end
end
