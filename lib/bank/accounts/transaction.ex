defmodule Bank.Accounts.Transaction do
  @moduledoc """
  Transaction schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Bank.Accounts.Account

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "transactions" do
    field :amount, :integer
    field :description, :string
    field :type, TransactionEnum
    belongs_to :source_account, Account
    belongs_to :destination_account, Account

    timestamps()
  end

  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:type, :amount, :description, :source_account_id, :destination_account_id])
    |> validate_required([:type, :amount, :source_account_id, :destination_account_id])
    |> validate_number(:amount, greater_than: 0)
    |> assoc_constraint(:source_account)
    |> assoc_constraint(:destination_account)
  end
end
