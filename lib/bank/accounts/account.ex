defmodule Bank.Accounts.Account do
  @moduledoc """
  Account schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Bank.Accounts.Transaction
  alias Bank.Accounts.User

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "accounts" do
    field :type, AccountEnum
    has_one :user, User
    has_many :source_transactions, Transaction, foreign_key: :source_account_id
    has_many :destination_transactions, Transaction, foreign_key: :destination_account_id

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [])
    |> validate_required([])
  end
end
