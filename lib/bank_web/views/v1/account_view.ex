defmodule BankWeb.V1.AccountView do
  use BankWeb, :view

  def render("account.json", %{account: account}) do
    %{
      id: account.id
    }
  end
end
