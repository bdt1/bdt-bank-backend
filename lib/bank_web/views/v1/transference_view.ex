defmodule BankWeb.V1.TransferenceView do
  use BankWeb, :view
  alias BankWeb.V1.TransferenceView
  alias BankWeb.V1.UserView

  def render("index.json", %{transferences: transferences}) do
    render_many(transferences, TransferenceView, "transference.json")
  end

  def render("show.json", %{transference: transference}) do
    render_one(transference, TransferenceView, "transference.json")
  end

  def render("transference.json", %{transference: transference}) do
    %{
      id: transference.id,
      amount: transference.amount,
      description: transference.description,
      inserted_at: transference.inserted_at,
      source_user: user(transference.source_account.user),
      destination_user: user(transference.destination_account.user)
    }
  end

  defp user(user) do
    render_one(user, UserView, "user.json")
  end
end
