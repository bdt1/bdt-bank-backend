defmodule BankWeb.V1.StatementView do
  use BankWeb, :view

  alias BankWeb.V1.TransferenceView

  def render("statements.json", %{statements: statements}) do
    list = render_many(statements.list, TransferenceView, "transference.json")
    Map.put(statements, :list, list)
  end
end
