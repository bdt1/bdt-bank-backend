defmodule BankWeb.V1.UserView do
  use BankWeb, :view
  alias BankWeb.V1.AccountView
  alias BankWeb.V1.UserView

  def render("index.json", %{users: users}) do
    render_many(users, UserView, "user.json")
  end

  def render("show.json", %{user: user}) do
    render_one(user, UserView, "user.json")
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      name: user.name,
      account: render_one(user.account, AccountView, "account.json")
    }
  end
end
