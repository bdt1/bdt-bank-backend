defmodule BankWeb.Backoffice.UserBackofficeSessionController do
  use BankWeb, :controller

  alias Bank.Backoffice
  alias BankWeb.Backoffice.UserBackofficeAuth

  def new(conn, _params) do
    render(conn, "new.html", error_message: nil)
  end

  def create(conn, %{"user_backoffice" => user_backoffice_params}) do
    %{"email" => email, "password" => password} = user_backoffice_params

    if user_backoffice = Backoffice.get_user_backoffice_by_email_and_password(email, password) do
      UserBackofficeAuth.log_in_user_backoffice(conn, user_backoffice, user_backoffice_params)
    else
      render(conn, "new.html", error_message: "Invalid email or password")
    end
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Logged out successfully.")
    |> UserBackofficeAuth.log_out_user_backoffice()
  end
end
