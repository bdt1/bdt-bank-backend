defmodule BankWeb.Backoffice.UserBackofficeSettingsController do
  use BankWeb, :controller

  alias Bank.Backoffice
  alias BankWeb.Backoffice.UserBackofficeAuth

  plug :assign_email_and_password_changesets

  def edit(conn, _params) do
    render(conn, "edit.html")
  end

  def update(conn, %{"action" => "update_email"} = params) do
    %{"current_password" => password, "user_backoffice" => user_backoffice_params} = params
    user_backoffice = conn.assigns.current_user_backoffice

    case Backoffice.apply_user_backoffice_email(user_backoffice, password, user_backoffice_params) do
      {:ok, applied_user_backoffice} ->
        Backoffice.deliver_update_email_instructions(
          applied_user_backoffice,
          user_backoffice.email,
          &Routes.user_backoffice_settings_url(conn, :confirm_email, &1)
        )

        conn
        |> put_flash(
          :info,
          "A link to confirm your email change has been sent to the new address."
        )
        |> redirect(to: Routes.user_backoffice_settings_path(conn, :edit))

      {:error, changeset} ->
        render(conn, "edit.html", email_changeset: changeset)
    end
  end

  def update(conn, %{"action" => "update_password"} = params) do
    %{"current_password" => password, "user_backoffice" => user_backoffice_params} = params
    user_backoffice = conn.assigns.current_user_backoffice

    case Backoffice.update_user_backoffice_password(
           user_backoffice,
           password,
           user_backoffice_params
         ) do
      {:ok, user_backoffice} ->
        conn
        |> put_flash(:info, "Password updated successfully.")
        |> put_session(
          :user_backoffice_return_to,
          Routes.user_backoffice_settings_path(conn, :edit)
        )
        |> UserBackofficeAuth.log_in_user_backoffice(user_backoffice)

      {:error, changeset} ->
        render(conn, "edit.html", password_changeset: changeset)
    end
  end

  def confirm_email(conn, %{"token" => token}) do
    case Backoffice.update_user_backoffice_email(conn.assigns.current_user_backoffice, token) do
      :ok ->
        conn
        |> put_flash(:info, "Email changed successfully.")
        |> redirect(to: Routes.user_backoffice_settings_path(conn, :edit))

      :error ->
        conn
        |> put_flash(:error, "Email change link is invalid or it has expired.")
        |> redirect(to: Routes.user_backoffice_settings_path(conn, :edit))
    end
  end

  defp assign_email_and_password_changesets(conn, _opts) do
    user_backoffice = conn.assigns.current_user_backoffice

    conn
    |> assign(:email_changeset, Backoffice.change_user_backoffice_email(user_backoffice))
    |> assign(:password_changeset, Backoffice.change_user_backoffice_password(user_backoffice))
  end
end
