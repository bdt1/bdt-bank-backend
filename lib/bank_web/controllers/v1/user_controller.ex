defmodule BankWeb.V1.UserController do
  use BankWeb, :controller

  alias Bank.Accounts
  alias Bank.Accounts.User

  alias Bank.Helper

  action_fallback BankWeb.FallbackController

  def index(conn, %{"search_field" => search_field}) do
    with users <- Accounts.get_user_autocomplete(search_field) do
      conn
      |> put_status(:ok)
      |> render("index.json", users: users)
    end
  end

  def index(conn, _params) do
    users = Accounts.list_users()
    conn
    |> put_status(:ok)
    |> render("index.json", users: users)
  end

  def create(conn, user_params) do
    with {:ok, %User{} = user} <-
           user_params
           |> Helper.Map.atomize_keys()
           |> Accounts.create_user() do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.v1_user_path(conn, :show, user))
      |> render("show.json", user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, %User{} = user} <- Accounts.get_user(id) do
      render(conn, "show.json", user: user)
    end
  end

  def update(conn, params) do
    with {:ok, %User{} = user} <- Accounts.get_user(params["id"]),
         {:ok, %User{} = user} <- Accounts.update_user(user, params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %User{} = user} <- Accounts.get_user(id),
         {:ok, %User{}} <- Accounts.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
