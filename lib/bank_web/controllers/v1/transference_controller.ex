defmodule BankWeb.V1.TransferenceController do
  use BankWeb, :controller

  alias Bank.Accounts
  alias Bank.Accounts.Transaction

  alias Bank.Helper

  action_fallback BankWeb.FallbackController

  def create(conn, transference_params) do
    with {:ok, %{transaction: %Transaction{} = transference}} <-
           transference_params
           |> Helper.Map.atomize_keys()
           |> Accounts.create_transference() do
      conn
      |> put_status(:created)
      |> render("show.json", transference: transference)
    end
  end
end
