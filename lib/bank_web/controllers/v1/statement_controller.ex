defmodule BankWeb.V1.StatementController do
  use BankWeb, :controller

  import Ecto.Query, warn: false

  alias Bank.Accounts.Transaction
  alias Bank.Pagination

  action_fallback BankWeb.FallbackController

  def index(conn, %{"page" => page, "per_page" => per_page}) do
    with {:ok, statements} <- Pagination.page(full_depth_transactions(), page, per_page: per_page) do
      conn
      |> put_status(:ok)
      |> render("statements.json", statements: statements)
    end
  end

  def show(conn, %{"page" => page, "per_page" => per_page, "account_id" => account_id}) do
    with {:ok, statements} <-
           Pagination.page(statements_from(account_id), page, per_page: per_page) do
      conn
      |> put_status(:ok)
      |> render("statements.json", statements: statements)
    end
  end

  defp full_depth_transactions() do
    from t in Transaction,
      preload: [
        source_account: [user: :account],
        destination_account: [user: :account]
      ]
  end

  defp statements_from(account_id) do
    from t in full_depth_transactions(),
      where: t.source_account_id == ^account_id,
      or_where: t.destination_account_id == ^account_id
  end
end
