defmodule BankWeb.Router do
  use BankWeb, :router

  # Admin
  import BankWeb.Backoffice.UserBackofficeAuth

  use Kaffy.Routes,
    scope: "/admin",
    pipe_through: [:browser, :require_authenticated_user_backoffice]

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user_backoffice
  end

  ## Admin Authentication routes

  scope "/", BankWeb.Backoffice do
    pipe_through [:browser, :redirect_if_user_backoffice_is_authenticated]

    get "/users_backoffice/log_in", UserBackofficeSessionController, :new
    post "/users_backoffice/log_in", UserBackofficeSessionController, :create
    get "/users_backoffice/reset_password", UserBackofficeResetPasswordController, :new
    post "/users_backoffice/reset_password", UserBackofficeResetPasswordController, :create
    get "/users_backoffice/reset_password/:token", UserBackofficeResetPasswordController, :edit
    put "/users_backoffice/reset_password/:token", UserBackofficeResetPasswordController, :update
  end

  scope "/", BankWeb.Backoffice do
    pipe_through [:browser, :require_authenticated_user_backoffice]

    get "/users_backoffice/settings", UserBackofficeSettingsController, :edit
    put "/users_backoffice/settings", UserBackofficeSettingsController, :update

    get "/users_backoffice/settings/confirm_email/:token",
        UserBackofficeSettingsController,
        :confirm_email
  end

  scope "/", BankWeb.Backoffice do
    pipe_through [:browser]

    delete "/users_backoffice/log_out", UserBackofficeSessionController, :delete
    get "/users_backoffice/confirm", UserBackofficeConfirmationController, :new
    post "/users_backoffice/confirm", UserBackofficeConfirmationController, :create
    get "/users_backoffice/confirm/:token", UserBackofficeConfirmationController, :confirm
  end

  # Admin /

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BankWeb do
    pipe_through :api

    scope "/v1", V1, as: :v1 do
      resources "/user", UserController

      get "/statements", StatementController, :index
      get "/statements/:account_id", StatementController, :show

      post "/transference", TransferenceController, :create
    end
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: BankWeb.Telemetry
    end
  end
end
