#!/usr/bin/env bash

bdt::start_web_service() {
  mix ecto.migrations || {
    mix ecto.create \
      && mix ecto.migrate \
      && mix run priv/repo/seeds.exs
  } || {
      echo "Error at pre-running application step."
      exit 1
  }

  exec mix phx.server
}

case "$1" in
  start_web_service)
    bdt::start_web_service
    ;;
  *)
    exec "${@}"
    ;;
esac

