defmodule BankWeb.StatementControllerTest do
  use BankWeb.ConnCase

  import Bank.Factory
  import Phoenix.View

  alias Bank.Helper
  alias BankWeb.V1.TransferenceView

  setup %{conn: conn} do
    {:ok,
     conn: put_req_header(conn, "accept", "application/json"),
     transactions: insert_list(4, :transaction)}
  end

  describe "index" do
    test "list page 1", %{conn: conn, transactions: transactions} do
      conn = get(conn, Routes.v1_statement_path(conn, :index), %{page: 1, per_page: 2})

      assert %{
               "count" => 4,
               "first_index" => 1,
               "has_next" => true,
               "has_prev" => false,
               "last_index" => 2,
               "list" => list,
               "next_page" => 2,
               "page" => 1,
               "prev_page" => 0
             } = json_response(conn, 200)

      assert Helper.Map.atomize_keys(list) ==
               transactions
               |> Enum.slice(0, 2)
               |> render_many(TransferenceView, "transference.json")
    end

    test "list page 2", %{conn: conn, transactions: transactions} do
      conn = get(conn, Routes.v1_statement_path(conn, :index), %{page: 2, per_page: 2})

      assert %{
               "count" => 4,
               "first_index" => 3,
               "has_next" => false,
               "has_prev" => true,
               "last_index" => 4,
               "list" => list,
               "next_page" => 3,
               "page" => 2,
               "prev_page" => 1
             } = json_response(conn, 200)

      assert Helper.Map.atomize_keys(list) ==
               transactions
               |> Enum.slice(2, 4)
               |> render_many(TransferenceView, "transference.json")
    end
  end

  describe "show" do
    test "list page 1", %{conn: conn, transactions: transactions} do
      %{destination_account_id: account_id} = List.first(transactions)

      conn = get(conn, Routes.v1_statement_path(conn, :show, account_id), %{page: 1, per_page: 5})

      assert %{
               "count" => 1,
               "first_index" => 1,
               "has_next" => false,
               "has_prev" => false,
               "last_index" => 1,
               "list" => list,
               "next_page" => 2,
               "page" => 1,
               "prev_page" => 0
             } = json_response(conn, 200)

      assert Helper.Map.atomize_keys(list) ==
               transactions
               |> Enum.slice(0, 1)
               |> render_many(TransferenceView, "transference.json")
    end
  end
end
