defmodule BankWeb.TransferenceControllerTest do
  use BankWeb.ConnCase

  import Bank.Factory

  alias Bank.Helper

  describe "create transference" do
    test "renders transference when data is valid", %{conn: conn} do
      [source_user, destination_user] = insert_list(2, :user)
      insert(:transaction, amount: 10, destination_account: source_user.account)

      attrs = %{
        amount: 10,
        description: "Test transaction",
        source_account_id: source_user.account.id,
        destination_account_id: destination_user.account.id
      }

      conn = post(conn, Routes.v1_transference_path(conn, :create), attrs)

      response =
        conn
        |> json_response(201)
        |> Helper.Map.atomize_keys()

      assert is_binary(response.id)
      assert response.amount == attrs.amount
      assert response.description == attrs.description
      assert response.destination_account_id == attrs.destination_account_id
      assert response.source_account_id == attrs.source_account_id
    end

    test "renders error with insuficient amount", %{conn: conn} do
      [source_user, destination_user] = insert_list(2, :user)

      attrs = %{
        amount: 10,
        description: "Test transaction",
        source_account_id: source_user.account.id,
        destination_account_id: destination_user.account.id
      }

      conn = post(conn, Routes.v1_transference_path(conn, :create), attrs)

      assert json_response(conn, 422)["errors"] == %{"amount" => ["insuficient credit"]}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.v1_transference_path(conn, :create), %{})

      assert json_response(conn, 422)["errors"] == %{
               "amount" => ["can't be blank"],
               "destination_account_id" => ["can't be blank"],
               "source_account_id" => ["can't be blank"]
             }
    end
  end
end
