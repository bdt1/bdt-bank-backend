defmodule BankWeb.UserControllerTest do
  use BankWeb.ConnCase

  import Bank.Factory
  import Phoenix.View

  alias Bank.Accounts.User

  @invalid_attrs %{email: nil, name: nil, nickname: nil, password: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json"), user: insert(:user)}
  end

  describe "index" do
    test "lists all users", %{conn: conn, user: user} do
      conn = get(conn, Routes.v1_user_path(conn, :index))

      assert length(json_response(conn, 200)) == 1

      response =
        conn
        |> json_response(200)
        |> List.first()
        |> Bank.Helper.Map.atomize_keys()

      assert response.id == user.id
      assert response.name == user.name
    end
  end

  describe "create user" do
    test "renders user when data is valid", %{conn: conn} do
      attrs = params_for(:user)
      conn = post(conn, Routes.v1_user_path(conn, :create), attrs)

      response =
        conn
        |> json_response(201)
        |> Bank.Helper.Map.atomize_keys()

      assert is_binary(response.id)
      assert response.name == attrs.name
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.v1_user_path(conn, :create), %{})

      assert json_response(conn, 422)["errors"] == %{"name" => ["can't be blank"]}
    end
  end

  describe "show user" do
    test "renders user when id exists", %{conn: conn, user: %User{id: id} = user} do
      conn = get(conn, Routes.v1_user_path(conn, :show, id))

      response =
        conn
        |> json_response(200)
        |> Bank.Helper.Map.atomize_keys()

      assert response.id == user.id
      assert response.name == user.name
    end

    test "renders error when id do not exists", %{conn: conn} do
      invalid_id = Ecto.UUID.generate()
      conn = get(conn, Routes.v1_user_path(conn, :show, invalid_id))

      assert json_response(conn, 404) == %{"errors" => %{"detail" => "Not Found"}}
    end

    test "abc", %{conn: conn} do
      alias BankWeb.V1.UserView

      user1 = insert(:user, name: "Alameda Caos")
      user2 = insert(:user, name: "Caos Teto")
      conn = get(conn, Routes.v1_user_path(conn, :index), %{search_field: "aos"})

      assert render_many([user1, user2], UserView, "user.json") == Bank.Helper.Map.atomize_keys(json_response(conn, 200))
    end
  end

  describe "update user" do
    test "renders user when data is valid", %{conn: conn, user: %User{id: id} = user} do
      attrs = params_for(:user)

      assert user.name != attrs.name
      conn = put(conn, Routes.v1_user_path(conn, :update, id), attrs)

      response =
        conn
        |> json_response(200)
        |> Bank.Helper.Map.atomize_keys()

      assert response.id == user.id
      assert response.name == attrs.name
    end

    test "renders errors when data is invalid", %{conn: conn, user: %User{id: id}} do
      conn = put(conn, Routes.v1_user_path(conn, :update, id), @invalid_attrs)

      assert json_response(conn, 422)["errors"] == %{"name" => ["can't be blank"]}
    end
  end

  describe "delete user" do
    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete(conn, Routes.v1_user_path(conn, :delete, user))
      assert response(conn, 204)

      conn = get(conn, Routes.v1_user_path(conn, :show, user))
      assert json_response(conn, 404) == %{"errors" => %{"detail" => "Not Found"}}
    end

    test "renders error when id do not exists", %{conn: conn} do
      invalid_id = Ecto.UUID.generate()
      conn = delete(conn, Routes.v1_user_path(conn, :delete, invalid_id))

      assert json_response(conn, 404) == %{"errors" => %{"detail" => "Not Found"}}
    end
  end
end
