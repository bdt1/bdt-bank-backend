defmodule Bank.AccountFactory do
  @moduledoc false

  alias Bank.Accounts.Account
  alias Faker

  defmacro __using__(_opts) do
    quote do
      def account_factory do
        %Account{
          type: :normal
        }
      end
    end
  end
end
