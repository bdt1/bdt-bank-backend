defmodule Bank.TransactionFactory do
  @moduledoc false

  alias Bank.Accounts.Transaction
  alias Faker.{Random, Team}

  defmacro __using__(_opts) do
    quote do
      def transaction_factory do
        %Transaction{
          amount: Random.Elixir.random_between(1, 1_000_000),
          description: Team.PtBr.name(),
          type: :transfer,
          source_account: build(:account),
          destination_account: build(:account)
        }
      end
    end
  end
end
