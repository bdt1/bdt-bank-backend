defmodule Bank.UserFactory do
  @moduledoc false

  alias Bank.Accounts.User
  alias Faker.Person

  defmacro __using__(_opts) do
    quote do
      def user_factory do
        %User{
          name: Person.PtBr.name(),
          account: build(:account)
        }
      end
    end
  end
end
