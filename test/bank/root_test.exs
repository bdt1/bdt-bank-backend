defmodule Bank.RootTest do
  use Bank.DataCase

  import Bank.Factory

  alias Bank.Accounts
  alias Bank.Root

  describe "add_credit/2" do
    test "add credit to valid account" do
      account = insert(:account)

      assert Accounts.balance(account.id) == 0

      Root.add_credit(account.id, 10)

      assert Accounts.balance(account.id) == 10
    end
  end
end
