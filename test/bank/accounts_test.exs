defmodule Bank.AccountsTest do
  use Bank.DataCase

  import Bank.Factory

  alias Bank.Accounts
  alias Bank.Repo
  alias Bank.Root

  describe "users" do
    alias Bank.Accounts.User

    test "list_users/0 returns all users" do
      users = insert_list(2, :user)

      assert Accounts.list_users() == users
    end

    test "get_user_autocomplete/1 returns a match" do
      user1 = insert(:user, name: "Alberto Costa")
      insert(:user, name: "Costa Lima")

      assert [user1] == "lberto" |> Accounts.get_user_autocomplete() |> Repo.preload(:account)
    end

    test "get_user_autocomplete/1 returns all matches" do
      user1 = insert(:user, name: "Alberto Costa")
      user2 = insert(:user, name: "Costa Lima")

      assert [user1, user2] == "osta" |> Accounts.get_user_autocomplete() |> Repo.preload(:account)
    end

    test "get_user/1 returns the user with given id" do
      user = insert(:user)

      assert Accounts.get_user(user.id) == {:ok, user}
    end

    test "create_user/1 with valid data creates a user" do
      params = params_for(:user)

      assert {:ok, %User{} = user} = Accounts.create_user(params)

      assert user.name == params.name
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(%{})
    end

    test "update_user/2 with valid data updates the user" do
      user = insert(:user)
      params = params_for(:user)

      assert {:ok, %User{} = updated_user} = Accounts.update_user(user, params)
      assert user != updated_user
      assert updated_user.name == params.name
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = insert(:user)
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, %{name: nil})
      assert {:ok, user} == Accounts.get_user(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = insert(:user)
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert Accounts.get_user(user.id) == {:error, :not_found}
    end
  end

  test "change_user/1 returns a user changeset" do
    user = insert(:user)
    assert %Ecto.Changeset{} = Accounts.change_user(user)
  end

  describe "accounts" do
    alias Bank.Accounts.Account

    test "create_user/1 with valid parameters create a valid account" do
      params = params_for(:user)

      assert {:ok, user} = Accounts.create_user(params)

      assert %Account{} = user.account
    end

    test "get_account/1 returns the account with given id" do
      account = insert(:account)

      assert Accounts.get_account(account.id) == {:ok, account}
    end

    test "get_root_account/1 returns the only root account" do
      account = Accounts.get_root_account()

      assert is_binary(account.id)
      assert account.type == :root
    end
  end

  describe "transactions" do
    alias Bank.Accounts.Transaction

    test "create_transaction/1 between valid accounts create a Transaction with given params" do
      [source_user, destination_user] = insert_list(2, :user)
      insert(:transaction, amount: 10, destination_account: source_user.account)

      attrs = %{
        amount: 10,
        description: "Test transaction",
        type: :transfer,
        source_account_id: source_user.account.id,
        destination_account_id: destination_user.account.id
      }

      assert {:ok, %{transaction: %Transaction{} = transaction}} =
               Accounts.create_transaction(attrs)

      assert is_binary(transaction.id)
      assert transaction.amount == attrs.amount
      assert transaction.source_account_id == attrs.source_account_id
      assert transaction.destination_account_id == attrs.destination_account_id
      assert transaction.type == attrs.type
      assert transaction.description == attrs.description
    end

    test "create_transaction/1 insuficient credit return changeset_error" do
      [source_user, destination_user] = insert_list(2, :user)

      attrs = %{
        amount: 10,
        type: :transfer,
        source_account_id: source_user.account.id,
        destination_account_id: destination_user.account.id
      }

      assert {:error, :validate_balance, %Ecto.Changeset{} = changeset,
              %{transaction: transaction}} = Accounts.create_transaction(attrs)

      assert errors_on(changeset) == %{amount: ["insuficient credit"]}
      assert Repo.get(Transaction, transaction.id) == nil
    end

    test "create_transaction/1 from invalid source and destination account return constraint error on source_account" do
      invalid_source_account_id = Ecto.UUID.generate()
      invalid_destination_account_id = Ecto.UUID.generate()

      attrs = %{
        amount: 10,
        type: :transfer,
        source_account_id: invalid_source_account_id,
        destination_account_id: invalid_destination_account_id
      }

      assert {:error, :transaction, %Ecto.Changeset{} = changeset, %{}} =
               Accounts.create_transaction(attrs)

      assert errors_on(changeset) == %{source_account: ["does not exist"]}
    end

    test "create_transaction/1 from valid source and invalid destination account return constraint error on destination_account" do
      source_user = insert(:user)
      invalid_destination_account_id = Ecto.UUID.generate()

      attrs = %{
        amount: 10,
        type: :transfer,
        source_account_id: source_user.account.id,
        destination_account_id: invalid_destination_account_id
      }

      assert {:error, :transaction, %Ecto.Changeset{} = changeset, %{}} =
               Accounts.create_transaction(attrs)

      assert errors_on(changeset) == %{destination_account: ["does not exist"]}
    end

    test "create_transaction/1 negative value to amount return changeset error" do
      [source_user, destination_user] = insert_list(2, :user)

      attrs = %{
        amount: -10,
        type: :transfer,
        source_account_id: source_user.account.id,
        destination_account_id: destination_user.account.id
      }

      assert {:error, :transaction, %Ecto.Changeset{} = changeset, %{}} =
               Accounts.create_transaction(attrs)

      assert errors_on(changeset) == %{amount: ["must be greater than 0"]}
    end

    test "create_transference/1 between valid accounts create a Transaction of type transfer" do
      [source_user, destination_user] = insert_list(2, :user)
      insert(:transaction, amount: 10, destination_account: source_user.account)

      attrs = %{
        amount: 10,
        description: "Test transaction",
        source_account_id: source_user.account.id,
        destination_account_id: destination_user.account.id
      }

      assert {:ok, %{transaction: %Transaction{} = transaction}} =
               Accounts.create_transference(attrs)

      assert is_binary(transaction.id)
      assert transaction.amount == attrs.amount
      assert transaction.source_account_id == attrs.source_account_id
      assert transaction.destination_account_id == attrs.destination_account_id
      assert transaction.type == :transfer
      assert transaction.description == attrs.description
    end

    test "balance/1" do
      [source_user, destination_user_1, destination_user_2] = insert_list(3, :user)

      Root.add_credit(source_user.account.id, 1000)
      assert Accounts.balance(source_user.account.id) == 1000

      attrs = %{
        amount: 50,
        description: "Test transaction",
        source_account_id: source_user.account.id,
        destination_account_id: destination_user_1.account.id
      }
      Accounts.create_transference(attrs)
      assert Accounts.balance(source_user.account.id) == 950

      attrs = %{
        amount: 50,
        description: "Test transaction 2",
        source_account_id: source_user.account.id,
        destination_account_id: destination_user_2.account.id
      }
      Accounts.create_transference(attrs)

      assert Accounts.balance(source_user.account.id) == 900
    end
  end
end
