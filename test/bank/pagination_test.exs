defmodule Bank.PaginationTest do
  use Bank.DataCase

  import Bank.Factory

  alias Bank.Accounts.User
  alias Bank.Pagination
  alias Bank.Repo

  describe "pagination" do
    test "page 1 per_page 2" do
      users = insert_list(4, :user)

      assert {:ok,
              %{
                count: 4,
                first_index: 1,
                has_next: true,
                has_prev: false,
                last_index: 2,
                list: list,
                next_page: 2,
                page: 1,
                prev_page: 0
              }} = Pagination.page(User, 1, per_page: 2)

      assert Repo.preload(list, :account) == Enum.slice(users, 0, 2)
    end
  end

  test "page 2 per_page 2" do
    users = insert_list(4, :user)

    assert {:ok,
            %{
              count: 4,
              first_index: 3,
              has_next: false,
              has_prev: true,
              last_index: 4,
              list: list,
              next_page: 3,
              page: 2,
              prev_page: 1
            }} = Pagination.page(User, 2, per_page: 2)

    assert Repo.preload(list, :account) == Enum.slice(users, 2, 4)
  end
end
