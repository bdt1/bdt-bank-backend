defmodule Bank.Helper.MapTest do
  use Bank.DataCase

  alias Bank.Helper

  describe "atomize_keys" do
    test "convert string keys to atoms" do
      assert Helper.Map.atomize_keys(%{"a" => 1, "b" => %{"c" => 2}}) ==
               %{a: 1, b: %{c: 2}}
    end

    test "misc string and atom keys to atoms" do
      assert Helper.Map.atomize_keys(%{"a" => 1, :b => %{"c" => 2}}) ==
               %{a: 1, b: %{c: 2}}
    end
  end
end
