defmodule Bank.Factory do
  @moduledoc false

  use ExMachina.Ecto, repo: Bank.Repo

  use Bank.AccountFactory
  use Bank.UserFactory
  use Bank.TransactionFactory
end
