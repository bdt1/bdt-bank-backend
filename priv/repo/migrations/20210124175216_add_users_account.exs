defmodule Bank.Repo.Migrations.AddUsersAccount do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :account_id, references(:accounts, type: :uuid), null: false
    end

    create unique_index(:users, [:account_id])
  end
end
