defmodule Bank.Repo.Migrations.CreateUsersBackofficeAuthTables do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS citext", ""

    create table(:users_backoffice, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :email, :citext, null: false
      add :hashed_password, :string, null: false
      add :confirmed_at, :naive_datetime
      timestamps()
    end

    create unique_index(:users_backoffice, [:email])

    create table(:users_backoffice_tokens, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :user_backoffice_id,
          references(:users_backoffice, type: :binary_id, on_delete: :delete_all),
          null: false

      add :token, :binary, null: false
      add :context, :string, null: false
      add :sent_to, :string
      timestamps(updated_at: false)
    end

    create index(:users_backoffice_tokens, [:user_backoffice_id])
    create unique_index(:users_backoffice_tokens, [:context, :token])
  end
end
