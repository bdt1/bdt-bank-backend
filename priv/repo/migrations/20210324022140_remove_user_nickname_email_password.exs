defmodule Bank.Repo.Migrations.RemoveUserNicknameEmailPassword do
  use Ecto.Migration

  def change do
    alter table(:users) do
      remove :nickname
      remove :password
      remove :email
    end
  end
end
