defmodule Bank.Repo.Migrations.PopulateUserBackoffice do
  use Ecto.Migration

  def change do
    [
      %{
        email: "admin@bdt.com.br",
        password: "arrasoubdtfloripa"
      }
    ]
    |> Enum.each(fn user ->
      {:ok, u} =
        user
        |> Bank.Backoffice.register_user_backoffice()

      u
      |> Bank.Backoffice.UserBackoffice.confirm_changeset()
      |> Bank.Repo.update()
    end)
  end
end
