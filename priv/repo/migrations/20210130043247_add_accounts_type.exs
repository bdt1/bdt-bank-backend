defmodule Bank.Repo.Migrations.AddAccountsType do
  use Ecto.Migration

  def change do
    alter table(:accounts) do
      add :type, :integer, default: 1
    end
  end
end
