# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Bank.Repo.insert!(%Bank.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
account =
  case Bank.Repo.get_by(Bank.Accounts.Account, type: 0) do
    %Bank.Accounts.Account{} = account -> account
    nil -> Bank.Repo.insert!(%Bank.Accounts.Account{type: 0})
  end

root_user_name = "Raíz BDT"
case Bank.Repo.get_by(Bank.Accounts.User, name: root_user_name) do
  %Bank.Accounts.User{} -> nil
  nil -> Bank.Repo.insert!(
      %Bank.Accounts.User{
        name: root_user_name,
        account_id: account.id
      }
    )
end
