# Bank

## Development setup

### Dockerized requirements:
1.  Ensure that you have Docker and Docker Compose locally.

### Local requirements:
1.  Ensure you have Elixir installed use [asdf](https://elixircasts.io/installing-elixir-with-asdf) to manage elixir version.

## Run with docker
1. `source .aliases`
2. `bank-setup`
3. `bank-up`

## Run local
1. Add to `/etc/hosts` the alias to local host `127.0.0.1    postgres`
2. `source .aliases`
3. Run the database docker `bank-up -d postgres`
4. `mix setup`
5. `iex -s mix phx.server`
